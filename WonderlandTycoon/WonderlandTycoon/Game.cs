﻿namespace WonderlandTycoon
{
    public class Game
    {
        private long score;
        private long money;
        private int nbRound;
        private int round = 1;
        private Map map;

        public Game(string name, int nbRound, long initialMoney)
        {
            TycoonIO.GameInit(name, nbRound, initialMoney);

            this.money = initialMoney;
            this.nbRound = nbRound;
            this.map = new Map(name);
        }

        public long Launch(Bot bot)
        {
            bot.Start(this);
            for (int i = 0; i < nbRound; i++)
            {
                bot.Update(this);
                Update();
                round += 1;
            }

            bot.End(this);
            return score;
        }

        public void Update()
        {
            money += map.GetIncome(map.GetPopulation());
            score += map.GetIncome(map.GetPopulation());
            TycoonIO.GameUpdate();
        }

        public bool Build(int i, int j, Building.BuildingType type)
        {
            if (map.Build(i, j, ref money, type))
            {
                TycoonIO.GameBuild(i, j, type);
                return true;
            }

            return false;
        }

        public bool Upgrade(int i, int j)
        {
            if (map.Upgrade(i, j, ref money))
            {
                TycoonIO.GameUpgrade(i, j);
                return true;
            }

            return false;
        }
        
        public bool Destroy(int i, int j)
        {
            return true;
            
        }

        public Map Map => map;

        public int Round => round;

        public long Score => score;

        public long Money => money;

        
    }
}
