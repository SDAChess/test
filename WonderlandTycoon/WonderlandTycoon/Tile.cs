﻿using System;
using System.Runtime.CompilerServices;

namespace WonderlandTycoon
{
    public class Tile
    {
        private Biome biome;
        private Building building;
        private int buildingLevel;

        public enum Biome
        {
            SEA,
            MOUNTAIN,
            PLAIN
        }

        public Building Building => building;

        public int BuildingLevel => buildingLevel;

        public Biome GetBiome()
        {
            return biome;
        }

        public Tile(Biome b)
        {
            biome = b;
            building = null;
        }

        public bool Build(ref long money, Building.BuildingType type)
        {
            if (building == null && biome == Biome.PLAIN)
            {
                switch (type)
                {
                    case Building.BuildingType.SHOP:
                        if (money < Shop.BUILD_COST) return false;
                        money -= Shop.BUILD_COST;
                        this.building = new Shop();
                        this.buildingLevel = 0;
                        return true;

                    case Building.BuildingType.HOUSE:
                        if (money < House.BUILD_COST) return false;
                        money -= House.BUILD_COST;
                        this.building = new House();
                        this.buildingLevel = 0;
                        return true;
                    case Building.BuildingType.ATTRACTION:
                        if (money < Attraction.BUILD_COST) return false;
                        money -= Attraction.BUILD_COST;
                        this.building = new Attraction();
                        this.buildingLevel = 0;
                        return true;
                }
            }

            return false;
        }

        public bool Upgrade(ref long money)
        {
            switch (building.Type)
            {
                case Building.BuildingType.NONE:
                    return false;
                case Building.BuildingType.HOUSE:
                    House house = (House) building;
                    if (!house.Upgrade(ref money)) return false;
                    buildingLevel++;
                    return true;

                case Building.BuildingType.SHOP:
                    Shop shop = (Shop) building;
                    if (!shop.Upgrade(ref money)) return false;
                    buildingLevel++;
                    return true;

                case Building.BuildingType.ATTRACTION:
                    Attraction attraction = (Attraction) building;
                    if (!attraction.Upgrade(ref money)) return false;
                    buildingLevel++;
                    return true;

            }

            return false;
        }

        public long GetHousing()
        {
            if (building is House house)
            {
                return house.Housing();
            }

            return 0;
        }

        public long GetAttractiveness()
        {
            if (building is Attraction attraction)
            {
                return attraction.Attractiveness();
            }

            return 0;
        }

        public long GetIncome(long population)
        {
            if (building is Shop shop)
            {
                return shop.Income(population);
            }

            return 0;
        }

        public bool IsBuildable()
        {
            if (building == null)
            {
                return biome == Biome.PLAIN;
            }
            return biome == Biome.PLAIN && building.Type == Building.BuildingType.NONE;
        }
        
    }
}