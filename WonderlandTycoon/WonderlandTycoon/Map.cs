﻿using System;
using System.CodeDom.Compiler;
using System.Numerics;
using System.Runtime.InteropServices.WindowsRuntime;

namespace WonderlandTycoon
{
    public class Map
    {
        private Tile[,] matrix;

        private int columns;
        private int rows;
        
        public Map(string name)
        {
            this.matrix = TycoonIO.ParseMap(name);
            this.columns = matrix.GetUpperBound(0);
            this.rows = matrix.GetLength(1);
        }

        public int Rows => rows;

        public Tile[,] Matrix => matrix;
        public int Columns => columns;
        
        public bool Build(int i, int j, ref long money,
                Building.BuildingType type)
        {
            if (i > columns || j >= rows)
            {
                return false;
            }
            Tile tile = matrix[i, j];
            if (tile != null && tile.GetBiome() == Tile.Biome.PLAIN)
            {
                return tile.Build(ref money, type);                
            }

            return false;
        }

        public bool Upgrade(int i, int j, ref long money)
        {
            Tile tile = matrix[i, j];
            return tile.Upgrade(ref money);
        }

        public long GetAttractiveness()
        {
            long attractiveness = 0;
            foreach (var tile in matrix)
            {
                attractiveness += tile.GetAttractiveness();
            }

            return attractiveness;
        }

        public long GetHousing()
        {
            long capacity = 0;
            foreach (var tile in matrix)
            {
                capacity += tile.GetHousing();
            }

            return capacity;
        }

        public long GetPopulation()
        {
            long housing = GetHousing();
            long attractiveness = GetAttractiveness();
            if (housing > attractiveness)
            {
                return attractiveness;
            }

            return housing;
        }

        public long GetIncome(long population)
        {
            long income = 0;
            foreach (var tile in matrix)
            {
                income += tile.GetIncome(population);
            }

            return income;
        }

        public int GetLvl(int p0, int p1)
        {
            Tile tile = matrix[p0, p1];
            return tile.BuildingLevel;

        }

        public (int, int) GetCoordinates(Tile tile)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] == tile)
                    {
                        return (i, j);
                    }
                }
            }

            return (0, 0);
        }
    }
}
