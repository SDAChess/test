﻿using System;

namespace WonderlandTycoon
{
    public class House : Building
    {
        public const long BUILD_COST = 250;
        public static readonly long[] UPGRADE_COST = {750, 3000, 10000};
        public static readonly long[] HOUSING = {300, 500, 650, 750};
        private int level;

        public House()
        {
            level = 0;
            this.type = BuildingType.HOUSE;
        }

        public long Housing()
        {
            return HOUSING[level];
        }

        public bool Upgrade(ref long money)
        {
            if (level <= 2 && money >= UPGRADE_COST[level])
            {
                money -= UPGRADE_COST[level];
                level += 1;
                return true;
            }

            return false;        
        }
    }
}
