﻿using System;

namespace WonderlandTycoon
{
    public class Shop : Building
    {
        public const long BUILD_COST = 300;
        public static readonly long[] UPGRADE_COST = {2500, 10000, 50000};
        public static readonly long[] INCOME = {7, 8, 9, 10};
        private int level;

        public Shop()
        {
            level = 0;
            this.type = BuildingType.SHOP;
        }
        
        public long Income(long population)
        {
            return (population * INCOME[level]) / 100;
        }

        public bool Upgrade(ref long money)
        {
            if (level <= 2 && money >= UPGRADE_COST[level])
            {
                money -= UPGRADE_COST[level];
                level += 1;
                return true;
            }

            return false;        
        }

        public int Level => level;
    }
}
