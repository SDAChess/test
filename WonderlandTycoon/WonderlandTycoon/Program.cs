﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Xml.Schema;

namespace WonderlandTycoon
{
    internal class Program
    {
        private const int NB_ROUNDS = 100;
        private const long INITIAL_MONEY = 11000;

        public static void Main(string[] args)
        {
            // Put your tests here like the example (don't forget to delete them before submitting)
            //TestsAttraction();

            //return;

            // These lines are launching the game and then the viewer
            /*Game game = new Game("agave_plantation.map", NB_ROUNDS, INITIAL_MONEY);
            long score = game.Launch(new MyBot());
            Console.WriteLine("Score: {0}", score);
            TycoonIO.Viewer();
            */
            long BestScore = 0;
            (int, int, int, int, float) config = (0, 0, 0, 0, 0);
            while(BestScore <= 25000000)
            {
                Random rand = new Random();
                Game game = new Game("agave_plantation.map", NB_ROUNDS, INITIAL_MONEY);
                var p1 = rand.Next(100);
                var p2 = rand.Next(p1,100);
                var p3 = rand.Next(p2,100);
                var p4 = rand.Next(p2,100);
                float p5 = (float) rand.Next(100) / 100;
                var score = game.Launch(new MyBot(p1, p2, p3, p4, p5));
                if (score >= BestScore)
                {
                    BestScore = score;
                    config = (p1, p2, p3, p4, p5);
                }
            }
            Console.WriteLine("Le meilleur score est :" + BestScore);
            Console.WriteLine("La config est " + config);
            
        }

        private static void TestsAttraction()
        {
            Attraction myFirstAttraction = new Attraction();
            long myMoney = INITIAL_MONEY;

            Console.WriteLine("Before upgrade of myFirstAttraction :");
            Console.WriteLine(" -> myFirstAttraction.Type = " + myFirstAttraction.Type + " (Should be: ATTRACTION)");
            Console.WriteLine(" -> myMoney = " + myMoney + " (Should be: 11000)");
            Console.WriteLine(" -> myFirstAttraction.Attractiveness() = " + myFirstAttraction.Attractiveness() 
                                                                          + " (Should be: 500)");
            myFirstAttraction.Upgrade(ref myMoney);

            Console.WriteLine();
            Console.WriteLine("After upgrade of myFirstAttraction :");
            Console.WriteLine(" -> myMoney = " + myMoney + " (Should be: 6000)");
            Console.WriteLine(" -> myFirstAttraction.Attractiveness() = " + myFirstAttraction.Attractiveness() 
                                                                          + " (Should be: 1000)");

            Console.WriteLine();
            Console.WriteLine("Try to upgrade with not enough money :");
            Console.WriteLine(" -> myFirstAttraction.Upgrade(ref myMoney) = " + myFirstAttraction.Upgrade(ref myMoney)
                                                                              + " (Should return: False)");

            long myFriendsMoney = 100000;
            Console.WriteLine();
            Console.WriteLine("Try to upgrade twice with more money :");
            Console.WriteLine(" -> " + myFirstAttraction.Upgrade(ref myFriendsMoney)
                                     + " " + myFirstAttraction.Upgrade(ref myFriendsMoney) 
                                     + " (Should be: True True)");
            Console.WriteLine("After upgrade of myFirstAttraction twice :");
            Console.WriteLine(" -> myFriendsMoney = " + myFriendsMoney + " (Should be: 45000)");
            Console.WriteLine(" -> myFirstAttraction.Attractiveness() = " + myFirstAttraction.Attractiveness() 
                                                                          + " (Should be: 1500)");

            Console.WriteLine();
            myMoney = 1000000;
            Console.WriteLine("Try to upgrade myFirstAttraction (can't more thant lvl3) :");
            Console.WriteLine(" -> myFirstAttraction.Upgrade(ref myMoney) = " + myFirstAttraction.Upgrade(ref myMoney) 
                                                                              + " (Should return: False)");
        }
    }
}
