using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;


namespace WonderlandTycoon
{
    public class MyBot : Bot
    {
        private List<(int, int)> shops = new List<(int, int)>();
        private List<(int, int)> houses = new List<(int, int)>();
        private List<(int, int)> attractions = new List<(int, int)>();
        private int p1;
        private int p2;
        private int p3;
        private int p4;
        private float p5;

        public MyBot(int p1, int p2, int p3, int p4, float p5)
        {
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
            this.p5 = p5;
        }

        public override void Start(Game game)
        {
            game.Build(0, 3, Building.BuildingType.ATTRACTION);
            game.Build(0, 2, Building.BuildingType.HOUSE);
            game.Build(0, 4, Building.BuildingType.HOUSE);
            attractions.Add((0,3));
            houses.Add((0,2));
            houses.Add((0,4));
        }

        public override void Update(Game game)
        {
            if (game.Round <= p1)
            {
                for (int i = 0; i < game.Map.Matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < game.Map.Matrix.GetLength(1); j++)
                    {
                        if (game.Build(i, j, Building.BuildingType.SHOP))
                        {
                            shops.Add((i, j));
                        }
                    }
                }
            }
            else if (game.Round >= p2)
            {
                if (game.Round <= p3 || game.Round >= p4)
                {
                    for (int k = 0; k < shops.Count; k++)
                    {
                        var (i, j) = shops[k];
                        game.Upgrade(i, j);   
                    }
                }
                else
                {
                    if (game.Map.GetAttractiveness() >= game.Map.GetHousing())
                    {
                        foreach (var t in houses)
                        {
                            var (i, j) = t;
                            game.Upgrade(i, j);
                        }
                    }
                    else
                    {
                        foreach (var t in attractions)
                        {
                            var (i, j) = t;
                            game.Upgrade(i, j);
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < game.Map.Matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < game.Map.Matrix.GetLength(1); j++)
                    {
                        if (game.Map.GetAttractiveness() > p5 * game.Map.GetHousing())
                        {
                            if (game.Build(i, j, Building.BuildingType.HOUSE))
                            {
                                houses.Add((i, j));
                            }
                        }
                        else
                        {
                            if (game.Build(i, j, Building.BuildingType.ATTRACTION))
                            {
                                attractions.Add((i, j));
                            }
                        }
                    }
                }
            }
        }

        public override void End(Game game)
        {
        }
    }
}