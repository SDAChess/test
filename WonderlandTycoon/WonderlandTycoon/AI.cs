using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Transactions;

namespace WonderlandTycoon
{
    public class AI : Bot
    {
        private long fitness;
        private List<(int, int)> freeTiles;
        private enum Action
        {
            BuildHouse, BuildAttraction, BuildShop, 
            UpgradeHouse, UpgradeAttraction, UpgradeShop
        }

        private List<Action> dna = new List<Action>();

        private readonly Action[] possibilities = {
            Action.BuildHouse, Action.BuildAttraction, Action.BuildShop, Action.UpgradeAttraction,
            Action.UpgradeHouse, Action.UpgradeShop
        };

        public AI(int actions)
        {
            GenerateDNA(actions);
        }

        public void PrintDna()
        {
            foreach (var gene in dna)
            {
                Console.WriteLine(gene.ToString());
            }
        }

        private void GenerateDNA(int actions)
        {
            for (int i = 0; i < actions; i++){
                int value = new Random().Next(6);
                dna.Add(possibilities[value]);
            }
        }

        private bool ActionManager(Game game, Action action, int i, int j)
        {
            switch (action)
            {
                case Action.BuildAttraction:
                    return game.Build(i, j, Building.BuildingType.ATTRACTION);
                case Action.BuildHouse:
                    return game.Build(i, j, Building.BuildingType.HOUSE);
                case Action.BuildShop:
                    return game.Build(i, j, Building.BuildingType.SHOP);
                case Action.UpgradeAttraction:
                case Action.UpgradeHouse:
                case Action.UpgradeShop:
                    return game.Upgrade(i, j);
            }

            return false;
        }
        public override void Start(Game game)
        {
            freeTiles = new List<(int, int)>();
            foreach (var tiles in game.Map.Matrix)
            {
                if (tiles.IsBuildable())
                {
                    var coordinates = game.Map.GetCoordinates(tiles);
                    if (coordinates != (0, 0))
                    {
                        freeTiles.Append(coordinates);
                    }
                }
            }
        }

        public override void Update(Game game)
        {
            for (int i = 0; i < game.Score; i++)
            {
                
            }
            foreach (var action in dna)
            {
                //ActionManager(game, action, i, j);
            }
        }

        public override void End(Game game)
        {
            fitness = game.Score;
        }
    }
}