﻿using System;

namespace WonderlandTycoon
{
    public class Attraction : Building
    {
        public const long BUILD_COST = 10000;
        public static readonly long[] UPGRADE_COST = {5000, 10000, 45000};
        public static readonly long[] ATTRACTIVENESS = {500, 1000, 1300, 1500};
        private int level;

        public Attraction()
        {
            level = 0;
            type = BuildingType.ATTRACTION;
        }

        public long Attractiveness()
        {
            return ATTRACTIVENESS[level];
        }

        public bool Upgrade(ref long money)
        {
            if (level <= 2 && money >= UPGRADE_COST[level])
            {
                money -= UPGRADE_COST[level];
                level += 1;
                return true;
            }

            return false;
        }

        public int Level => level;
    }
}
